import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lab_2/startGame.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

import 'main_screen.dart';
import 'map.dart';
import 'notes.dart';
import 'weather.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  SystemChrome.setPreferredOrientations(
          [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight])
      .whenComplete(() {
    runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: "/",
      routes: {
        "/": (context) => MyApp(),
        "/map": (context) => Map(),
        "/notes": (context) => Notes(),
        "/game": (context) => GameApp(),
        "/weather": (context) => Weather(),
      },
    ));
  });
}
