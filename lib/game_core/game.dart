import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:lab_2/game_core/main_loop.dart';

import '../utilits/GlobalVars.dart';

class Game extends StatefulWidget {
  const Game({Key? key}) : super(key: key);
  @override
  State<Game> createState() => _GameState();
}

class _GameState extends State<Game> {
  late ReceivePort _receivePort;
  late Isolate _isolateLoop;


  void _startIsolateLoop () async {
    _receivePort = ReceivePort();
    _isolateLoop = await Isolate.spawn(mainLoop, _receivePort.sendPort);
    _receivePort.listen((message) {
      GlobalVars.currentScene.update();
      GlobalVars.currentScene_2.update();
      setState(() {});
    });
  }

  @override
  void initState() {
    _startIsolateLoop();
    super.initState();

  }

  @override
  void dispose() {
    _receivePort.close();
    _isolateLoop.kill();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/background.png"),
          fit: BoxFit.cover,
        )
      ),
      child: GlobalVars.currentScene.buildScene(),
        // children: [
        //   GlobalVars.currentScene.buildScene(),
        //   // GlobalVars.currentScene_2.buildScene(),
        // ],
      // )
    );
  }
}
