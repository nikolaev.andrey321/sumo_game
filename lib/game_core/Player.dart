import 'package:flutter/cupertino.dart';

class Player {
  double _x = 0;
  double _y = 0;
  double _degree = 0;


  Widget build (String imgPath) {
    return Positioned(
        top: _x,
        left: _y,
        child: RotationTransition(
          turns: AlwaysStoppedAnimation(_degree / 360),
          child: Image.asset(imgPath, width: 50, height: 50,)
        )
    );
  }

  void spin() {
    _degree += 5;
    if (_degree == 360) {
      _degree = 0;
    }
  }

  void update() {
    spin();
  }

  set x(double value) {
    _x = value;
  }

  set y(double value) {
    _y = value;
  }


}