import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:lab_2/scenes/AppScene.dart';

import '../game_core/Player.dart';

class GameScene extends AppScene {
  String imgPath;
  Player _player = Player();

  GameScene(this.imgPath);



  @override
  Widget buildScene() {
    return Stack(
      children: [
        _player.build(this.imgPath)
      ],
    );
  }

  @override
  void update() {
    _player.update();
  }

  @override
  void x(double width) {
    _player.x = width;
  }

  @override
  void y(double height) {
    _player.y = height;
  }

}