import 'package:lab_2/scenes/AppScene.dart';

import '../scenes/GameScene.dart';

class GlobalVars {
  static AppScene currentScene = GameScene("assets/red_sumo.png");
  static AppScene currentScene_2 = GameScene("assets/blue_sumo.png");
  static double screenWidth = 0;
  static double screenHeight = 0;
}