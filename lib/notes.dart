import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

class Notes extends StatefulWidget {
  const Notes({Key? key}) : super(key: key);

  @override
  State<Notes> createState() => _NotesState();
}

class _NotesState extends State<Notes> {
  late String _userNote;
  List notesList = [];

  // void initFirebase() async {
  //   WidgetsFlutterBinding.ensureInitialized();
  //   await Firebase.initializeApp(
  //       options: DefaultFirebaseOptions.currentPlatform);
  // }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // initFirebase();

    notesList.addAll(["Фильмы", "Планы", "ДЗ"]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: Text("Заметочки"),
        centerTitle: true,
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection("notes").snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) return Text("Нет записей");
          return ListView.builder(
              itemCount: (snapshot.data!).docs.length,
              itemBuilder: (BuildContext context, int index) {
                return Dismissible(
                  key: Key((snapshot.data!).docs[index].id),
                  child: Card(
                    child: ListTile(
                      title: Text((snapshot.data!).docs[index].get("note")),
                      trailing: IconButton(
                        onPressed: () {
                          FirebaseFirestore.instance
                              .collection("notes")
                              .doc((snapshot.data!).docs[index].id)
                              .delete();
                        },
                        icon: Icon(
                          Icons.delete_sweep_outlined,
                          color: Colors.blueGrey,
                        ),
                      ),
                    ),
                  ),
                  onDismissed: (direction) {
                    FirebaseFirestore.instance
                        .collection("notes")
                        .doc((snapshot.data!).docs[index].id)
                        .delete();
                  },
                );
              });
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("Добавить заметку"),
                  content: TextField(
                    onChanged: (String value) {
                      _userNote = value;
                    },
                  ),
                  actions: [
                    ElevatedButton(
                        onPressed: () {
                          FirebaseFirestore.instance
                              .collection("notes")
                              .add({"note": _userNote});

                          Navigator.of(context).pop();
                        },
                        child: Text("ОК"))
                  ],
                );
              });
        },
        backgroundColor: Colors.purple,
        child: Icon(
          Icons.add,
        ),
      ),
    );
  }
}
