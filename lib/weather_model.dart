import 'network_data.dart';

const weatherApiUrl = 'https://api.openweathermap.org/data/2.5/weather';

class WeatherModel {
  Future<dynamic> getLocationWeather() async {
    /// await for methods that return future
    double lat = 55.994333;
    double lon = 92.797481;
    String apiKey = "f3733901cfe3e05d0e2eef3b9a64932b";

    /// Get location data
    ///&units=metric change the temperature metric
    NetworkData networkHelper = NetworkData(
        '$weatherApiUrl?lat=${lat}&lon=${lon}&appid=$apiKey&units=metric');
    var weatherData = await networkHelper.getData();
    return weatherData;
  }
}
