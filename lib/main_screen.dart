import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:weather/weather.dart';
// import 'network_data.dart';
import 'weather_model.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double lat = 55.994333;
    double lon = 92.797481;
    // int temperature;
    // // WeatherFactory wf = new WeatherFactory("f3733901cfe3e05d0e2eef3b9a64932b");
    // WeatherModel weatherModel = WeatherModel();

    // getLocationData() async {
    //   var weatherData = await weatherModel.getLocationWeather();
    //   double temp = weatherData['main']['temp'];
    //   final int temperature = temp.toInt();
    //   print("ABOBA $temperature");
    //   return temperature;
    // }

    // final int temperature = getLocationData() as int;
    const MaterialColor white = MaterialColor(
      0xFFFFFFFF,
      <int, Color>{
        50: Color(0xFFFFFFFF),
        100: Color(0xFFFFFFFF),
        200: Color(0xFFFFFFFF),
        300: Color(0xFFFFFFFF),
        400: Color(0xFFFFFFFF),
        500: Color(0xFFFFFFFF),
        600: Color(0xFFFFFFFF),
        700: Color(0xFFFFFFFF),
        800: Color(0xFFFFFFFF),
        900: Color(0xFFFFFFFF),
      },
    );
    final mainButtonStyle = ButtonStyle(
        backgroundColor: MaterialStateProperty.all(Colors.blue),
        foregroundColor: MaterialStateProperty.all(Colors.white),
        overlayColor: MaterialStateProperty.all(Colors.deepPurple),
        // padding:MaterialStateProperty.all(const EdgeInsets.all(20)),
        minimumSize: MaterialStateProperty.all(const Size(130, 130)),
        shape: MaterialStateProperty.all(const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          side: BorderSide(color: Colors.black, width: 5),
        )));
    const num123Style = TextStyle(
      fontSize: 40,
    );

    final additionalButtons = ButtonStyle(
      padding: MaterialStateProperty.all(const EdgeInsets.all(5)),
      minimumSize: MaterialStateProperty.all(const Size(50, 50)),
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: white,
        scaffoldBackgroundColor: Colors.purple,
      ),
      home: Scaffold(
          body: SafeArea(
              child: Container(
        margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 60),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  style: additionalButtons,
                  onPressed: () {
                    Navigator.pushNamed(context, "/weather");
                  },
                  child: Text("Погода"),
                ),
                Image.asset(
                  'assets/sumo.png',
                  width: 75,
                  height: 75,
                ),
                ElevatedButton(
                  style: additionalButtons,
                  onPressed: () {
                    Navigator.pushNamed(context, "/map");
                  },
                  child: Image.asset(
                    'assets/map.png',
                    width: 35,
                    height: 35,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  // backgroundColor: MaterialStateProperty.all(Colors.redAccent),
                  style: mainButtonStyle,
                  onPressed: () {
                    Navigator.pushNamed(context, "/game");
                  },
                  child: Column(
                    children: const [
                      Text(
                        "2",
                        style: num123Style,
                      ),
                      Text("Игрока"),
                    ],
                  ),
                ),
                ElevatedButton(
                  style: mainButtonStyle,
                  onPressed: () {},
                  child: Column(
                    children: const [
                      Text(
                        "3",
                        style: num123Style,
                      ),
                      Text("Игрока"),
                    ],
                  ),
                ),
                ElevatedButton(
                  style: mainButtonStyle,
                  onPressed: () {},
                  child: Column(
                    children: const [
                      Text(
                        "4",
                        style: num123Style,
                      ),
                      Text("Игрока"),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FloatingActionButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/notes");
                  },
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0))),
                  child: const Icon(Icons.edit_note),
                ),
              ],
            ),
          ],
        ),
      ))

          // floatingActionButton: FloatingActionButton(
          //   onPressed: () {  },
          //   child: Icon(Icons.edit_note),
          //   shape: RoundedRectangleBorder(
          //       borderRadius: BorderRadius.all(Radius.circular(15.0))
          //   ),
          //
          // ),

          ),
    );
  }
}


//
// class MyApp extends StatelessWidget {
//   const MyApp({super.key});
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: const MyHomePage(title: 'Flutter Demo Home Page'),
//     );
//   }
// }
//
// class MyHomePage extends StatefulWidget {
//   const MyHomePage({super.key, required this.title});
//   final String title;
//
//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }
//
// class _MyHomePageState extends State<MyHomePage> {
//   int _counter = 0;
//
//   void _incrementCounter() {
//     setState(() {
//       _counter++;
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             const Text(
//               'You have pushed the button this many times:',
//             ),
//             Text(
//               '$_counter',
//               style: Theme.of(context).textTheme.headlineMedium,
//             ),
//           ],
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: _incrementCounter,
//         tooltip: 'Increment',
//         child: const Icon(Icons.add),
//       ), // This trailing comma makes auto-formatting nicer for build methods.
//     );
//   }
// }
