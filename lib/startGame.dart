import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lab_2/utilits/GlobalVars.dart';

import 'game_core/game.dart';

// void main () {
//   WidgetsFlutterBinding.ensureInitialized();
//   SystemChrome.setPreferredOrientations([
//     DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight
//   ])
//       .whenComplete(() {
//     SystemChrome.setEnabledSystemUIOverlays([
//       SystemUiOverlay.bottom
//     ]);
//     runApp(
//         MaterialApp(
//           debugShowCheckedModeBanner: false,
//           home: SafeArea(
//             child: Scaffold(
//               body: MyApp(),
//             ),
//           )
//         )
//     );
//   });
// }

class GameApp extends StatefulWidget {
  const GameApp({Key? key}) : super(key: key);


  @override
  State<GameApp> createState() => _GameAppState();
}

class _GameAppState extends State<GameApp> {

  @override
  void didChangeDependencies() {
    initGame();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Game();
  }

  void initGame() {
    GlobalVars.screenWidth = MediaQuery.of(context).size.width;
    GlobalVars.screenHeight = MediaQuery.of(context).size.height;
    GlobalVars.currentScene.x(GlobalVars.screenHeight / 2 + GlobalVars.screenHeight * 0.1);
    GlobalVars.currentScene.y(GlobalVars.screenWidth / 2 );

    GlobalVars.currentScene_2.x(0);
    GlobalVars.currentScene_2.y(0);
    // GlobalVars.currentScene_2.x(GlobalVars.screenHeight / 2 - GlobalVars.screenHeight * 0.1);
    // GlobalVars.currentScene_2.y(GlobalVars.screenWidth / 2);
  }
}
